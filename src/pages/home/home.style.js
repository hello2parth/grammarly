const getStyle = () => ({
  innerContainer:{
    flexGrow: '1',
    maxWidth: '1080px',
  },
  outerContainer: {
    width: '100vw',
    display: 'flex',
    justifyContent: 'center',
  }
})

export default getStyle;

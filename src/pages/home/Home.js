import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

import Features from './components/features';
import Plans from './components/plans';
import CustomerResults from './components/customerResults';
import getStyle from './home.style';

function Home({classes}) {
  return (
    <div className={classes.outerContainer}>
      <div className={classes.innerContainer}>
        <Features></Features>
        <Plans></Plans>
        <CustomerResults></CustomerResults>
      </div>
    </div>
  );
}

Home.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(getStyle)(Home);

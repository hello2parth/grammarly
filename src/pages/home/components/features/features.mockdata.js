import communicationTitle from '../../../../assets/svg/communicationTitle.svg';
import communicationSubTitle from '../../../../assets/svg/communicationSubTitle.svg';
import comprehensiveTitle from '../../../../assets/svg/comprehensiveTitle.svg';
import comprehensiveSubTitle from '../../../../assets/svg/comprehensiveSubTitle.svg';
import supportTitle from '../../../../assets/svg/supportTitle.svg';
import supportSubTitle from '../../../../assets/svg/supportSubTitle.svg';
import toneTitle from '../../../../assets/svg/toneTitle.svg';
import toneSubTitle from '../../../../assets/svg/toneSubTitle.svg';

export const FEATURES_MOCKDATA = [
  {
    title: 'Clear, confident communication',
    subTitle: 'Take the guesswork out of great writing.',
    titleImg: communicationTitle,
    subTitleImg: communicationSubTitle,
  },
  {
    title: 'Comprehensive real-time feedback',
    subTitle: 'Effective writing takes more than good grammar.',
    titleImg: comprehensiveTitle,
    subTitleImg: comprehensiveSubTitle,
  },
  {
    title: 'Support you can rely on',
    subTitle: 'Write with a second pair of eyes that never gets tired.',
    titleImg: supportTitle,
    subTitleImg: supportSubTitle,
  },
  {
    title: 'Strike the right tone',
    subTitle: 'Make the best impression, every time.',
    titleImg: toneTitle,
    subTitleImg: toneSubTitle,
  },
]

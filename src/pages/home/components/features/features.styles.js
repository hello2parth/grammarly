import feature from '../../../../assets/background/feature.png';

const getStyle = (theme) => ({
  backgroundContainer: {
    background: `url(${feature})`,
    width: '100vw',
    marginLeft: 'calc((100% - 100vw) / 2)',
    justifyContent: 'center',
    [theme.breakpoints.up('sm')]: {
      minHeight: '680px',
    },
  },
  featuresInnerContainer: {
    display: 'flex',
    paddingBottom: '60px',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '590px',
    [theme.breakpoints.down('sm')]: {
      flexFlow: 'column',
      padding: '0 25px',
      marginBottom: '24px',
    },
  },
  descriptionContainer:{
    maxWidth: '354.81px',
  },
  progressItem: {
    width: '320px',
  },
  description: {
    paddingTop: '24px',
    fontSize: '16px',
    lineHeight: '28px',
  },
  titleActive: {
    fontSize: '16px !important',
    lineHeight: '24px !important',
    color: 'rgb(14, 16, 26) !important', // when selected
    fontWeight: '700 !important',
  },
  titleInActive: {
    fontSize: '16px !important',
    lineHeight: '24px !important',
    color: 'rgb(74, 110, 224) !important', // when not selected
    fontWeight: '700 !important',
  },
  subTitle: {
    fontSize: '14px',
    lineHeight: '22px',
    color: 'rgb(109, 117, 141) !important',
    paddingBottom: '16px',
  },
  descriptionItemContainer: {
    marginTop: '20px',
    cursor: 'pointer',
  },
  imageContainer: {
    maxWidth: '640px',
    display: 'flex',
    flexGrow: '1',
    justifyContent: 'center',
    flexDirection: 'column',
    [theme.breakpoints.down('sm')]: {
      maxWidth: 'calc(100vw - 48px)',
      paddingTop: '48px',
    },
  },
  titleImage: {
    maxWidth: '388px',
    maxHeight: '250px',
    [theme.breakpoints.up('sm')]: {
      marginLeft: '50px',
    },
    boxShadow: '0 14px 55px rgb(109 117 141 / 20%)',
  },
  subTitleImage: {
    maxWidth: '434px',
    maxHeight: '138px',
    marginLeft: '100px',
    marginTop: '-50px',
    boxShadow: '0 14px 55px rgb(109 117 141 / 20%)',
    // to do: add transistion
    // to do: fix margin top issue for different image height by adding the container
  }
})

export default getStyle;

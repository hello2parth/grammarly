import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

import Header from '../header';
import Description from './components/Description';
import getStyle from './features.styles';
import { FEATURES_MOCKDATA } from './features.mockdata';

const MAX_PROGRESS = 100;

function Features({classes}) {
  const [progress, setProgress] = React.useState(0);
  const [activeItem, setActiveItem] = React.useState(0);

  useEffect(() => {
    const timer = setInterval(() => {
      setProgress((prevProgress) => (prevProgress >= MAX_PROGRESS ? MAX_PROGRESS : prevProgress + 1));
    }, 75);

    return () => {
      clearInterval(timer);
    };
  }, []);

  useEffect(() => {
    if(progress === MAX_PROGRESS) {
      setActiveItem(prevActiveItem =>  prevActiveItem === FEATURES_MOCKDATA.length - 1 ? 0 : prevActiveItem + 1);
      setProgress(0);
    }
  },[progress])

  const updateActiveItem = (i) => {
    if(i !== activeItem) {
      setActiveItem(i);
      setProgress(0);
    }
  }

  const {titleImg, subTitleImg} = FEATURES_MOCKDATA[activeItem];
  return (
    <div className={classes.backgroundContainer}>
      <Header></Header>
      <div className={classes.featuresInnerContainer}>
        <div className={classes.imageContainer}>
          <img className={classes.titleImage} src={titleImg} alt="title"/>
          <img className={classes.subTitleImage} src={subTitleImg} alt="subTitle"/>
        </div>
        <div className={classes.descriptionContainer}>
          {FEATURES_MOCKDATA.map(({title, subTitle}, i) =>
            <Description
              classes={classes}
              isActive={activeItem === i}
              progress={progress}
              title={title}
              subTitle={subTitle}
              onClick={() => updateActiveItem(i)}
              key={title}
            />
          )}
        </div>
      </div>
    </div>
  )
}

Features.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(getStyle)(Features);

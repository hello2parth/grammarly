import React from "react";
import PropTypes from 'prop-types';

import { Typography } from '@mui/material';

import LinearProgress from "../../../../../molecules/linearProgress";

function Description({title, subTitle, classes, isActive, progress, onClick}) {
  return(
    <div className={classes.descriptionItemContainer} onClick={onClick} role="presentation">
      <Typography variant="h5" component="div" classes={{h5: isActive ? classes.titleActive : classes.titleInActive}}>
        {title}
      </Typography>
      <Typography variant="content" component="div" classes={{content: classes.subTitle}}>
        {subTitle}
      </Typography>
      <LinearProgress progress={isActive ? progress : 0} key={isActive ? 1 : 0}/>
    </div>
  );
}

Description.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  isActive: PropTypes.bool.isRequired,
  progress: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default Description;

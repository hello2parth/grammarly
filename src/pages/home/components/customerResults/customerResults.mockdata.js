export const RESULTS_MOCKDATA = [
  {
    value: 76,
    description: '76% of Grammarly users find writing more enjoyable.'
  },
  {
    value: 85,
    description: '85% of Grammarly users are now stronger writers'
  },
  {
    value: 70,
    description: '70% of Grammarly users reported an increased level of writing confidence.'
  },
]

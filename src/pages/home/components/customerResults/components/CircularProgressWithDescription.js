import React from 'react';
import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import CircularProgressWithLabel from '../../../../../molecules/circularProgressWithLabel/CircularProgressWithLabel';
import getStyle from '../customerResults.styles';

function CircularProgressWithDescription({finalValue, description, classes}){
  const [progress, setProgress] = React.useState(0);

  React.useEffect(() => {
    const timer = setInterval(() => {
      setProgress((prevProgress) => (prevProgress >= finalValue ? finalValue : prevProgress + 1));
    }, 50);
    return () => {
      clearInterval(timer);
    };
  }, [finalValue]);

  return (
    <div className={classes.progressItem}>
      <CircularProgressWithLabel value={progress} />
      <Typography variant="content" component="div" classes={{content: classes.description}}>
       {description}
      </Typography>
    </div>
  );
}

CircularProgressWithDescription.propTypes = {
  finalValue: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
}

export default withStyles(getStyle)(CircularProgressWithDescription);

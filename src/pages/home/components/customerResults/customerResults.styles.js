const getStyle = (theme) => ({
  backgroundContainer: {
    background: '#f9faff',
    width: '100vw',
    marginLeft: 'calc((100% - 100vw) / 2)',
  },
  container: {
    maxWidth: '1080px',
    paddingBottom: '40px',
    margin: '0 auto',
    [theme.breakpoints.down('sm')]: {
      maxWidth: 'calc(100vw - 64px)',
      padding: '0 32px',
    },
  },
  progressContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    [theme.breakpoints.down('sm')]: {
      flexFlow: 'column',
    },
  },
  progressItem: {
    width: '320px',
    [theme.breakpoints.down('sm')]: {
      padding: '16px',
    },
  },
  description: {
    paddingTop: '24px',
    fontSize: '16px',
    lineHeight: '28px',
  },
  title: {
    fontSize: '29px !important',
    letterSpacing: '-.003em !important',
    lineHeight: '38px !important',
    color: '#0E101A !important',
    fontWeight: '700 !important',
  },
  subtitle: {
    paddingTop: '16px',
    fontSize: '18px',
    lineHeight: '32px',
    color: '#0E101A!important',
    paddingBottom: '72px',
    [theme.breakpoints.down('sm')]: {
      paddingBottom: '16px',
    },
  }
})

export default getStyle;

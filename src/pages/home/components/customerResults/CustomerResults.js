import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@mui/material/Typography';

import CircularProgressWithDescription from './components/CircularProgressWithDescription';
import getStyle from './customerResults.styles';
import { RESULTS_MOCKDATA } from './customerResults.mockdata';

function CustomerResults({classes}) {
  return (
    <div className={classes.backgroundContainer}>
      <div className={classes.container}>
        <Typography variant="h4" component="div" classes={{h4: classes.title}}>
          Premium Customers Report Better Results
        </Typography>
        <Typography variant="content" component="div" classes={{content: classes.subtitle}}>
          We routinely survey Grammarly users and have found that:
        </Typography>
        <div className={classes.progressContainer}>
          {RESULTS_MOCKDATA.map(({value, description}) =>
            <CircularProgressWithDescription
              key={value}
              finalValue={value}
              description={description}
            ></CircularProgressWithDescription>)}
        </div>
      </div>
    </div>
  );
}

CustomerResults.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(getStyle)(CustomerResults);

import checkmark from '../../../../assets/svg/checkmark.svg';

const getStyle = (theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '16px',
    marginBottom: '60px',
    [theme.breakpoints.down('sm')]: {
      maxWidth: '100vw',
      marginBottom: '0px',
    },
  },
  title: {
    fontSize: '29px !important',
    letterSpacing: '-.003em !important',
    lineHeight: '38px !important',
    color: '#0E101A !important',
    fontWeight: '700 !important',
    textAlign: 'center',
  },
  subtitle: {
    fontSize: '18px',
    lineHeight: '32px',
    color: '#0E101A!important',
    textAlign: 'center',
    padding: '8px 16px 48px 16px',
  },
  planItemsContainer: {
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
  planItem: {
    width: '337px',
    padding: '0 32px',
    display: 'flex',
    boxSizing: 'border-box',
    flexDirection: 'column',
    [theme.breakpoints.down('sm')]: {
      marginBottom: '30px',
    },
  },
  planItemTop: {
    display: 'flex',
    flexDirection: 'column',
    height: '170px',
    [theme.breakpoints.down('sm')]: {
      height: 'auto',
    },
  },
  audience: {
    fontSize: '11px !important',
    letterSpacing: '-.04em !important',
    lineHeight: '16px !important',
    color: 'rgb(109, 117, 141) !important',
  },
  planTitle: {
    fontSize: '24px !important',
    lineHeight: '32px !important',
    color: 'rgb(14, 16, 26) !important',
    fontWeight: '700 !important',
    paddingTop: '4px',
  },
  planDescription: {
    fontSize: '15px !important',
    lineHeight: '24px !important',
    color: 'rgb(14, 16, 26) !important',
    paddingTop: '4px',
  },
  button: {
    color: 'white !important',
    background: '#11a683 !important',
    paddingLeft: '0px!important',
    paddingRight: '0px!important',
    height: '48px',
    fontWeight: '700 !important',
    fontSize: '14px !important',
    lineHeight: '32px !important',
    textTransform: 'inherit !important',
    boxShadow: 'none !important',
    [theme.breakpoints.down('sm')]: {
      marginTop: '16px !important',
    },
    '&:hover': {
      background: '#15c39a !important',
    }
  },
  btnDisabled: {
    color: '#c6cbde !important',
    background: '#f0f2fc !important',
    paddingLeft: '0px!important',
    paddingRight: '0px!important',
    height: '48px',
    fontWeight: '700 !important',
    fontSize: '14px !important',
    lineHeight: '32px !important',
    textTransform: 'inherit !important',
    cursor: 'not-allowed !important',
    boxShadow: 'none !important',
    width: '100%',
    [theme.breakpoints.down('sm')]: {
      marginTop: '16px !important',
    },
  },
  list: {
    margin: 0,
    padding: '16px 0 0 0',
  },
  listItem: {
    listStyleImage: `url(${checkmark})`,
    marginBottom: '8px',
    marginLeft: '24px',
  },
  listItemContent: {
    borderBottom: '1px dotted #9fa6bf', // to-do: replace with liner gradient
    paddingBottom: '1px',
    fontSize: '14px !important',
    lineHeight: '22px !important',
    color: '#0E101A !important',
  },
  flexGrow: {
    flexGrow: '1',
  },
  flex: {
    display: 'flex',
  },
  tooltipContainer: {
    minHeight: '0 !important',
    background: '#333954 !important',
    fontSize: '12px !important',
    lineHeight: '18px !important',
    marginBottom: '0 !important',
    marginTop: '0 !important',
  },
  tooltipPopper: {
    background: '#333954 !important',
    borderRadius: '4px',
    maxWidth: '200px',
    padding: '16px !important',
  },
  tooltipPopperForImage: {
    background: '#333954 !important',
    borderRadius: '4px',
    maxWidth: '392px',
    padding: '8px !important',
  },
  tooltipImage: {
    maxWidth: '100%',
    marginBottom: '8px',
  }
})

export default getStyle;

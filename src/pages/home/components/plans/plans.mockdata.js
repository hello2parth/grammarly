import Spelling from '../../../../assets/svg/spelling.svg';
import Grammar from '../../../../assets/svg/grammer.svg';
import Punctuation from '../../../../assets/svg/punctuation.svg';
import Conciseness from '../../../../assets/svg/conciseness.svg';
import SentenceRewrites from '../../../../assets/svg/sentenceRewrites.svg';
import ToneAdjustment from '../../../../assets/svg/toneAdjustment.svg';
import WordChoice from '../../../../assets/svg/wordChoice.svg';
import Formality from '../../../../assets/svg/formality.svg';
import Fluency from '../../../../assets/svg/fluency.svg';

export const PLANS_MOCKDATA = [
  {
    audience: 'FOR CASUAL WRITING',
    title: 'Free',
    description: 'Basic writing suggestions.',
    button: {
      text: 'Included In Premium',
      disabled: true,
    },
    features:[
      { title: 'Spelling', image: Spelling, bold: false, tooltip: 'Eliminate spelling errors.'},
      { title: 'Grammar', image: Grammar, bold: false, tooltip: 'Eliminate grammatical errors.' },
      { title: 'Punctuation', image: Punctuation, bold: false, tooltip: 'Eliminate punctuation errors.' },
      { title: 'Conciseness', image: Conciseness, bold: false, tooltip: 'Make every sentence concise and easy to follow.' },
    ]
  },
  {
    audience: 'FOR WORK OR SCHOOL',
    title: 'Premium',
    description: 'Style, tone, and clarity improvements for writing at work and school.',
    button: {
      text: 'Upgrade to Grammarly Premium',
      disabled: false,
    },
    features: [
      { title: 'Everything in Free', image: null, bold: true },
      { title: 'Clarity-focused sentence rewrites', image: SentenceRewrites, bold: false,
        tooltip: 'Automatically rewrite hard-to-read sentences.'
      },
      { title: 'Tone adjustments', image: ToneAdjustment, bold: false,
        tooltip: 'Eliminate hedging language or unnecessary qualifiers to sound more confident.'
      },
      { title: 'Plagiarism detection', image: null, bold: false,
        tooltip: 'Ensure your work is fresh and original by checking it against 16 billion web pages.',
      },
      { title: 'Word choice', image: WordChoice, bold: false,
        tooltip: 'Find vivid words to enliven each and every message.',
      },
      { title: 'Formality level', image: Formality, bold: false,
        tooltip: "Write with the appropriate tone, even when you're in a hurry."
      },
      { title: 'Fluency', image: Fluency, bold: false,
        tooltip: 'Ensure your word choices sound natural and fluent.',
      },
      { title: 'Additional advanced suggestions', image: null, bold: false,
        tooltip: 'Fix inconsistencies in spelling and punctuation, adjust the tone of your writing, and get additional advanced feedback.'
      },
    ]
  }
];

import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import Tooltip from '@mui/material/Tooltip';

// To do: remove transistion of MUI tooltip

function Plan ({classes, item}) {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

  const renderListItem = ({title, image, bold, tooltip}) => {
    if(bold) {
      return <strong><span className={classes.listItemContent}>{title}</span></strong>
    }
    if(image) {
      return (
        <Tooltip title={
          <>
            <img className={classes.tooltipImage} src={image} alt="subTitle"/>
            <span>{tooltip}</span>
          </>
          } placement="top" classes={{ tooltip: classes.tooltipContainer, popper: classes.tooltipPopperForImage }}>
          <div><span className={classes.listItemContent}>{title}</span></div>
        </Tooltip>
      );
    }
    if(tooltip) {
      return (
        <Tooltip title={tooltip} placement="top" classes={{ tooltip: classes.tooltipContainer, popper: classes.tooltipPopper }}>
          <div><span className={classes.listItemContent}>{title}</span></div>
        </Tooltip>
      );
    }
   return <div><span className={classes.listItemContent}>{title}</span></div>
  }

  const {
    audience,
     title,
     description,
     button,
     features,
  } = item;

  return (
    <div className={classes.planItem}>
      <div className={classes.planItemTop}>
        <Typography variant="caption" component="div" classes={{caption: classes.audience}}>
          {audience}
        </Typography>
        <Typography variant="h5" component="div" classes={{h5: classes.planTitle}}>
          {title}
        </Typography>
        <Typography variant="content" component="div" classes={{content: classes.planDescription}}>
          {description}
        </Typography>
        {!isMobile && <div className={classes.flexGrow}></div> }
        <Button variant="contained" classes={{root: button.disabled ? classes.btnDisabled : classes.button}}>{button.text}</Button>
      </div>
      <ul className={classes.list}>
        {features.map(({title, image, bold, tooltip}) =>
          <li key={title} className={classes.listItem}>
            {renderListItem({title, image, bold, tooltip})}
          </li>
        )}
      </ul>
    </div>
  );
}

Plan.propTypes = {
  classes: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
}

export default Plan;

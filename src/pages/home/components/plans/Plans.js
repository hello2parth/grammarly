import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@mui/material/Typography';

import Plan from './components/Plan';
import getStyle from './plans.style';
import { PLANS_MOCKDATA } from './plans.mockdata';

function Plans({classes}) {
  return (
    <div className={classes.container}>
      <Typography variant="h4" component="div" classes={{h4: classes.title}}>
        Up-Level Your Communication
      </Typography>
      <Typography variant="content" component="div" classes={{content: classes.subtitle}}>
        Unlock Grammarly Premium’s advanced features and suggestions.
      </Typography>
      <div className={classes.planItemsContainer}>
        {PLANS_MOCKDATA.map(item => <Plan item={item} key={item.title} classes={classes}></Plan>)}
      </div>
    </div>
  );
}


Plans.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(getStyle)(Plans);

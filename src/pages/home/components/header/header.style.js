const getStyle = () => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: '32px',
  },
  headerText: {
    color: '#0E101A',
    fontSize: '42px !important',
    letterSpacing: '-.005em !important',
    lineHeight: '52px !important',
    fontWeight: '700 !important',
    paddingBottom: '24px',
    textAlign: 'center',
  },
  button: {
    color: 'white !important',
    background: '#11a683 !important',
    paddingLeft: '22px!important',
    paddingRight: '22px!important',
    height: '48px',
    fontWeight: '700 !important',
    fontSize: '14px !important',
    lineHeight: '32px !important',
    boxShadow: 'none !important',
    textTransform: 'inherit !important',
    '&:hover': {
      background: '#15c39a !important',
    }
  }
})

export default getStyle;

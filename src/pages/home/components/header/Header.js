import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@mui/material';
import Button from '@mui/material/Button';

import getStyle from './header.style';

function Header({classes}) {
  return (
    <div className={classes.container}>
      <Typography variant="h2" component="div" classes={{h2: classes.headerText}}>
        Aim High With Brilliant Writing
      </Typography>
      <Button variant="contained" classes={{root: classes.button}}>Upgrade to Grammarly Premium</Button>
    </div>
  )
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(getStyle)(Header);

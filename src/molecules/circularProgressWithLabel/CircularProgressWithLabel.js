import React from 'react';
import PropTypes from 'prop-types';

import CircularProgress from '@mui/material/CircularProgress';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { withStyles } from '@material-ui/core/styles';

import getStyle from './circularProgressWithLabel.style';

// to do: fill the circle by default, not available in mui

function CircularProgressWithLabel({classes, ...props}) {
  return (
    <Box sx={{ position: 'relative', display: 'inline-flex' }}>
      <CircularProgress variant="determinate" {...props} size={146} thickness={1.4} />
      <Box
        sx={{
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          position: 'absolute',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <div className={classes.textAndPercentageContent}>
          <Typography variant="h4" component="div" classes={{h4: classes.text}}>
            {`${Math.round(props.value)}`}
          </Typography>
          <Typography variant="h6" component="div" classes={{h6: classes.percentage}}>
            {`%`}
          </Typography>
        </div>
      </Box>
    </Box>
  );
}

CircularProgressWithLabel.propTypes = {
  value: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(getStyle)(CircularProgressWithLabel);

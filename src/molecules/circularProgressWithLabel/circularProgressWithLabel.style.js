const getStyle = () => ({
  text: {
    color: '#0E101A',
    fontSize: '40px',
    fontWeight: '700 !important',
  },
  percentage: {
    color: '#0E101A',
    fontSize: '30px',
  },
  textAndPercentageContent: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline'
  },
})

export default getStyle;

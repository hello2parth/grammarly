import * as React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Box from '@mui/material/Box';
import MUILinearProgress from '@mui/material/LinearProgress';

import getStyle from './linearProgress.styles';

function LinearProgress({classes, progress}) {
  return (
    <Box sx={{ width: '100%' }}>
      <MUILinearProgress variant="determinate" value={progress} classes={{
        root: classes.root,
        bar: classes.bar
      }}/>
    </Box>
  );
}

LinearProgress.propTypes = {
  classes: PropTypes.object.isRequired,
  progress: PropTypes.number.isRequired,
}

export default withStyles(getStyle)(LinearProgress);

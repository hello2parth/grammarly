const getStyle = () => ({
  root: {
    backgroundColor: '#e7e9f5 !important',
    height: '2px !important',
  },
  bar: {
    backgroundColor: '#0e101a !important',
  }
})

export default getStyle;
